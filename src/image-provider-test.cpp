#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>

#include "provider.h"

int main(int argc, char *argv[])
{
    QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));

    QScopedPointer<QQuickView> view(SailfishApp::createView());

    CustomImageProvider pdfImageProvider;
    view->engine()->addImageProvider(QStringLiteral("CUSTOM_PROVIDER"), &pdfImageProvider);
    view->setSource(SailfishApp::pathToMainQml());
    view->show();

    return app->exec();
}
