#include "provider.h"

CustomImageProvider::CustomImageProvider() : QQuickImageProvider(QQuickImageProvider::Image)
{ }

QImage CustomImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize) {
    Q_UNUSED(id)
    Q_UNUSED(size)
    Q_UNUSED(requestedSize)
    return QImage();
}
