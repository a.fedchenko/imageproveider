#ifndef PROVIDER_H
#define PROVIDER_H

#include <QQuickImageProvider>

class CustomImageProvider : public QQuickImageProvider
{
public:
    CustomImageProvider();
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;
};

#endif // PROVIDER_H
